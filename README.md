# Labview utilities

In this repo you will find some utility code that we used in
Labview. Specifically some XControls that implement a PID loop,
displaying the average of a number, and some helper function that add
some I/O functionality.

This code was written and used around 2011 and has not been updated
since then. So your milage may vary ;)

The code has not been tested using newer LabView versions.

We do not plan to provide updates and bugfixes for this code at this
stage.

See the Copyright notice and License for details.

